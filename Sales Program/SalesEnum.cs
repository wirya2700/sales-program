﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sales_Program
{
    class SalesEnum : IEnumerator<SalesMan>
    {
        private Int16 Position;
        private SalesMan[] sales;

        public SalesEnum(SalesMan[] _sales)
        {
            Position = -1;
            sales = new SalesMan[_sales.Length];

            for (Int16 i = 0; i < _sales.Length;i++ )
            {
                sales[i] = _sales[i];
            }
        }

        public SalesMan Current
        {
            get
            {
                return sales[Position];
            }
        }

        object IEnumerator.Current
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public bool MoveNext()
        {
            Position++;
            return (Position < sales.Length);
        }

        public void Dispose()
        {

        }
        public void Reset()
        {
            Position = -1;
        }
    }
}
