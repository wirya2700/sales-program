﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sales_Program;

namespace Sales_Program
{
    public class ListSalesMan
    {
        public static List<SalesMan> salesMans = new List<SalesMan>(){
                            new SalesMan(){Id =1, Name="Andi"},
                            new SalesMan(){Id =2, Name="Budi"},
                            new SalesMan(){Id =3, Name="Cindi"},
                            new SalesMan(){Id =4, Name="Santi"},
                            new SalesMan(){Id =5, Name="Pitri"}
                    };
        public ListSalesMan()
        {
            

            var _sales = (from tb_salesMan in salesMans
                          select tb_salesMan
                          );

            Console.WriteLine("NO \t ID \t Name");
            Console.WriteLine("============================");
            Int16 no = 0;
            foreach (var _salesMan in _sales)
            {
                no++;
                Console.WriteLine("{2} \t {0} \t {1}", _salesMan.Id , _salesMan.Name , no);
            }
            Console.WriteLine("============================");
            Console.WriteLine(_sales.Count());
        }
        
    }
}
