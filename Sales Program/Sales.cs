﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sales_Program
{
    class Sales : IEnumerable<SalesMan>
    {
        private SalesMan[] sales;

        public Sales(SalesMan[] _sales)
        {
            sales = new SalesMan[_sales.Length];
            for (Int16 i = 0; i < _sales.Length; i++)
            {
                sales[i] = _sales[i];
            }
        }

        public IEnumerator<SalesMan> GetEnumerator()
        {
            return new SalesEnum(sales);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return null;
        }
    }
}
