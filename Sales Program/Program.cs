﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sales_Program
{
    class Program
    {
        static void Main(string[] args)
        {
            tryAgain:
            Console.WriteLine("\nPlease input command \n1. List of Sales \n2. List of SalesMan\n3. Exit");
            try
            {
                Int16 i = Convert.ToInt16(Console.ReadLine());
                switch (i)
                {
                    case 1:
                        ListSales _listSales = new ListSales();
                        goto tryAgain;
                    case 2 :
                        ListSalesMan _listSalesMan = new ListSalesMan();
                        goto tryAgain;
                    case 3:
                        break;
                    default :
                        goto tryAgain;
                }
            }
            catch
            {
                Console.WriteLine();
                Console.WriteLine("Wrong input, Try Again ");
                goto tryAgain;
            }
            
        }
    }
}
