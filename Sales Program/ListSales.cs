﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sales_Program
{
    class ListSales
    {
        public ListSales()
        {
            List<Sale> sales = new List<Sale>()
            {
                new Sale(){Id = 1, Date=DateTime.Parse("12-02-2017"), _Sale=20000},
                new Sale(){Id = 1, Date=DateTime.Parse("12-02-2017"), _Sale=25000},
                new Sale(){Id = 2, Date=DateTime.Parse("12-03-2017"), _Sale=30000},
                new Sale(){Id = 2, Date=DateTime.Parse("12-03-2017"), _Sale=25000},
                new Sale(){Id = 3, Date=DateTime.Parse("12-05-2017"), _Sale=55000},
                new Sale(){Id = 4, Date=DateTime.Parse("12-05-2017"), _Sale=100000},
                new Sale(){Id = 5, Date=DateTime.Parse("12-05-2017"), _Sale=60000},
                new Sale(){Id = 5, Date=DateTime.Parse("12-05-2017"), _Sale=450000},
            };
            
            var _sales = (from tb_salesMan in ListSalesMan.salesMans
                          join tb_sales in sales on tb_salesMan.Id equals tb_sales.Id
                          where tb_sales._Sale < 50000
                          select new
                          {
                              _Id = tb_salesMan.Id,
                              _Name = tb_salesMan.Name,
                              _Date = tb_sales.Date.ToString("dd/MM/yyyy"),
                              _sale = tb_sales._Sale,
                          });

            Console.WriteLine("NO \t ID \t Name \t Date \t\t Sale");
            Console.WriteLine("===============================================");
            Int16 no = 0;
            foreach (var _salesMan in _sales)
            {
                no++;
                Console.WriteLine("{4} \t {0} \t {1} \t {2} \t {3}", _salesMan._Id, _salesMan._Name, _salesMan._Date, _salesMan._sale, no);
            }
            Console.WriteLine("===============================================");
            Console.WriteLine(_sales.Count() + "\t\t\t\t\t" + _sales.Sum(p => p._sale));
        }
        
    }
}
